Command line instructions<br>
You can also upload existing files from your computer using the instructions below.<br>
<br>
Git global setup<br>
git config --global user.name "Daniela Leyton"<br>
git config --global user.email "danielaleytongiraldo@gmail.com"<br>
<br>
Create a new repository<br>
git clone https://gitlab.com/o36-g01tiendavirtual/o36-g01-tiendavirtual.git<br>
cd o36-g01-tiendavirtual<br>
git switch -c main<br>
touch README.md<br>
git add README.md<br>
git commit -m "add README"<br>
git push -u origin main<br>
<br>
Push an existing folder<br>
cd existing_folder<br>
git init --initial-branch=main<br>
git remote add origin https://gitlab.com/o36-g01tiendavirtual/o36-g01-tiendavirtual.git<br>
git add .<br>
git commit -m "Initial commit"<br>
git push -u origin main<br>
<br>
Push an existing Git repository<br>
cd existing_repo<br>
git remote rename origin old-origin<br>
git remote add origin https://gitlab.com/o36-g01tiendavirtual/o36-g01-tiendavirtual.git<br>
git push -u origin --all<br>
git push -u origin --tags<br>
